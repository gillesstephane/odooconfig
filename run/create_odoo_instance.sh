#!/bin/bash
set -e
set -u

echo
echo "
######### INSTANCE INSTALL erp ######## ########
#
#                 (__)
#                 (oo)
#           /------\/
#          / |    ||
#         *  /\---/\
#            ~~   ~~ 
#
########## ######### ######### ######### #######"
echo
echo
echo "This program is for install erp version (v - 8,9 or 10), do you want to continue by [y/n]:"
read response
re='^[0-9]+$'
#doc tab
# 0 : version name (odoo, openerp) 
# 1 : path odoo instance user (/opt/$erpUser/odoo)
# 2 : user already have odoo instance ($erpUser)
# 3 : version (7, 8, 9, 10)
tabVersionName[0]="version"
#write command to exe in user profile
USER_HOME=$(eval echo ~${SUDO_USER})
touch $USER_HOME/commandrun
sudo chmod a+rwx $USER_HOME/commandrun
#TODO : sed don't work on [program:supervisor-oneplus-server]
if ([ $response == "y" ] || [ "$response" == "Y" ] || [ "$response" == "yes" ] || [ "$response" == "Yes" ] || [ "$response" == "YES" ]); then
	echo "Lest start !"
	#update system repository before start
	sudo apt-get update
	echo "Python deps installation"
	#python deps 
	#postgres install
	postgresql_version() {
		> $USER_HOME/commandrun
		if [ -d /etc/postgresql ]; then
			sudo psql -V | egrep -o '[0-9]{1,}\.[0-9]{1,}' > $USER_HOME/commandrun
		fi
	}	

	pgadmin_installed() {
		> $USER_HOME/commandrun
		sudo find /usr/ -name 'pgadm*' > $USER_HOME/commandrun
	}

	pip_cache() {
		> $USER_HOME/commandrun
		#test if pip cache ~/.bashrc			
		if grep -q 'export PIP_DOWNLOAD_CACHE*' $USER_HOME/.bashrc; then
		    echo "found"  > $USER_HOME/commandrun
		else
		    echo "notfound" > $USER_HOME/commandrun
		fi

		cmd=$(head -n 1 $USER_HOME/commandrun)
		if [ ! -z "$cmd" ];then
			if [ "$cmd" = "notfound" ];then
				echo "export PIP_DOWNLOAD_CACHE='~/.pip/cache';" >> $USER_HOME/.bashrc
			fi
		fi
	}

	postgresql_version
	#install postgresql	
	if [ ! -s  $USER_HOME/commandrun ]; then
		sudo apt-get -y install --no-install-recommends postgresql postgresql-contrib
	fi
	#install pip && add futures downloads in cache
	sudo apt-get -y install --no-install-recommends python-setuptools python-pip supervisor nginx git build-essential gcc python-dev libxml2-dev libxslt1-dev libjpeg62-dev libldap2-dev libsasl2-dev libssl-dev node-less libpq-dev 
	sudo pip install --upgrade pip
	pip_cache
	pip install virtualenv

	echo 
	echo "What is your user name [eg: mycompany]:"
	read erpUser

	
	run_deps_odoo_core (){
		echo "cd ~ && virtualenv venv-erpv"$erpVersion" && source venv-erpv"$erpVersion"/bin/activate && pip install -r ${tabVersionName[0]}/requirements.txt" > $USER_HOME/commandrun
		cmd=$(head -n 1 $USER_HOME/commandrun)
		sudo -H -u "$erpUser" /bin/bash -c "$cmd"
	}

	copy_odoo_local_core (){
		#don't clone odoo core if already exists for local user
		#read local users name
		sudo cut -d: -f1 /etc/passwd > $USER_HOME/commandrun
		for systemuserfile in $USER_HOME/commandrun; do
			#install deps
			while read systemuser; do
			  if [ -d /opt/$erpUser ];then
				if [ -d /opt/$erpUser/odoo ]; then
					if [ -f /opt/$systemuser/odoo_version.txt ]; then
						cmd=$(head -n 1 /opt/$systemuser/odoo_version.txt)
						if [ ! -z ${erpVersion+x} ]; then
							#"var is set"
							if [ $cmd == $erpVersion ]; then
								tabVersionName[1]="/opt/$systemuser/odoo"
								#user have instance odoo
								tabVersionName[2]="$systemuser"
								break
							fi
						fi
					fi 
				fi
			  fi
			done <$systemuserfile
		done
	}

	user_exist() {
		sudo cut -d: -f1 /etc/passwd > $USER_HOME/commandrun
		for systemuserfile in $USER_HOME/commandrun; do
			#install deps
			while read systemuser; do
			  if [ $systemuser == $erpUser ];then
				tabVersionName[2]="$systemuser"
				break
			  fi
			done <$systemuserfile
		done
	}

	create_user() {
		#copy_odoo_local_core
		user_exist
		if [ ! -z ${tabVersionName[2]+x} ]; then
			#"var is set"
			echo 
			echo "This user exist do you want to delete it ? yes/no "
			read delUser
			if [ $delUser == "yes" ]; then
				#del user
				if [ -d /opt/$erpUser ]; then
					sudo rm -R /opt/$erpUser
				fi
				if [ -f etc/supervisor/conf.d/supervisor-$erpUser-server.conf ]; then
					sudo rm /etc/supervisor/conf.d/supervisor-$erpUser-server.conf  
				fi
				if [ -f /etc/supervisor/conf.d/supervisor-$erpUser-server-longpolling.conf ]; then
					sudo rm /etc/supervisor/conf.d/supervisor-$erpUser-server-longpolling.conf
				fi
				#TODO : delete nginx config
				#create user and home (important to activate user shell)
				userdel -f $erpUser
				#TODO : delete databases user
				sudo adduser --system --home=/opt/"$erpUser" --group "$erpUser" --shell /bin/bash
				unset tabVersionName[2]
			fi
		else
			sudo adduser --system --home=/opt/"$erpUser" --group "$erpUser" --shell /bin/bash
		fi
	}

	#test user exists
	create_user
	if [ -z ${tabVersionName[2]+x} ]; then 
		copy_odoo_local_core
		echo "OpenERP version to install [7,8,9 or 10]"
		echo
		echo "What is your erp version [eg for Odoo: 8]:"
		read erpVersion	

		echo "create erp community repository or delete it"
		create_erp_erp (){ 

			  echo
			  echo "Now create repository erp begins."
			  echo
		
			if [ $erpVersion == "7" ]; then
				tabVersionName[0]="openerpv7"
				#get dependencies
				if [ ! -d /opt/$erpUser/openerpdeps ];then
					#TODO:adapt for copy_odoo_local_core()
					clear
					echo "cd ~ && virtualenv venv-erpv"$erpVersion" && source venv-erpv"$erpVersion"/bin/activate && git clone https://gillesstephane@bitbucket.org/gillesstephane/openerpdeps.git" > $USER_HOME/commandrun
					cmd=$(head -n 1 $USER_HOME/commandrun)
					sudo -H -u "$erpUser" /bin/bash -c "$cmd"
				fi
				#read files name
				dir="/opt/$erpUser/openerpdeps"
				for requirement in $dir/*.txt; do
					echo $requirement
					#install deps
					while read dep; do
					  echo $dep
						echo "cd ~ && virtualenv venv-erpv"$erpVersion" && source venv-erpv"$erpVersion"/bin/activate && apt-get -y install $dep" > $USER_HOME/commandrun
						cmd=$(head -n 1 $USER_HOME/commandrun)
						sudo -H -u root /bin/bash -c "$cmd"
					done <$requirement
				done

				#remove deps repository
				if [ ! -d /opt/$erpUser/openerpdeps ];then
					sudo rm -R /opt/$erpUser/openerpdeps
				fi

				#get erp deb
				if [ ! -f "/opt/$erpUser/openerp_7.0.latest.tar.gz" ];then
					sudo mkdir /opt/$erpUser/${tabVersionName[0]}
					sudo mkdir /opt/$erpUser/${tabVersionName[0]}/tmp
					sudo wget -c http://nightly.odoo.com/7.0/nightly/deb/openerp_7.0.latest.tar.gz -P /opt/$erpUser/${tabVersionName[0]}/tmp/
					sudo tar -xvf /opt/$erpUser/${tabVersionName[0]}/tmp/openerp_7.0.latest.tar.gz -C /opt/$erpUser/${tabVersionName[0]}/tmp/
					sudo cp -R /opt/$erpUser/${tabVersionName[0]}/tmp/odoo*/* /opt/$erpUser/${tabVersionName[0]}
					sudo rm -R /opt/$erpUser/${tabVersionName[0]}/tmp
					sudo touch /opt/$erpUser/odoo_version.txt
					sudo echo "$erpVersion" > /opt/$erpUser/odoo_version.txt
					sudo chown -R $erpUser:$erpUser /opt/$erpUser/${tabVersionName[0]}
					#sudo rm openerp_7.0.latest.tar.gz
				fi

			else
				tabVersionName[0]="odoo"
				if [ ! -d /opt/$erpUser/${tabVersionName[0]} ]; then
					sudo mkdir /opt/$erpUser/${tabVersionName[0]}
					sudo chown $erpUser:$erpUser /opt/$erpUser/${tabVersionName[0]}
				fi
				copy_odoo_local_core
				if [ ${#tabVersionName[@]} -gt 1 ]; then
					clear
					echo "---- You have one local user which have erp v$erpVersion ----"
					echo "local copy start..."
					sudo cp -R ${tabVersionName[1]} /opt/$erpUser/
					echo "local copy finish..."
					run_deps_odoo_core
				else
					clear
					echo "cd ~ && virtualenv venv-erpv"$erpVersion" && source venv-erpv"$erpVersion"/bin/activate && git clone https://www.github.com/${tabVersionName[0]}/${tabVersionName[0]} --depth 1 --branch "$erpVersion".0 --single-branch && pip install -r ${tabVersionName[0]}/requirements.txt" > $USER_HOME/commandrun
					cmd=$(head -n 1 $USER_HOME/commandrun)
					sudo -H -u "$erpUser" /bin/bash -c "$cmd"
					#odoo version on file (useful on next clone became local copy)
					sudo touch /opt/$erpUser/odoo_version.txt
					sudo echo "$erpVersion" > /opt/$erpUser/odoo_version.txt
				fi
				
			fi
		}
		delete_erp_erp (){ 

			  echo
			  echo "Now delete repository ${tabVersionName[0]} really begins."
			  echo

			 sudo rm -R "/opt/$erpUser/${tabVersionName[0]}" 
		}
		
		is_wkhtmltopdf_installed (){
			> $USER_HOME/commandrun
			if [ -d /etc/wkhtmltox ]; then
				sudo psql -V | egrep -o '[0-9]{1,}\.[0-9]{1,}' > $USER_HOME/commandrun
			fi
		}

		install_wkhtmltopdf (){
			#wkhtmltopdf
			if [ ! -d wkhtmltox ]; then
				sudo wget -c download.gna.org/wkhtmltopdf/0.12/0.12.3/wkhtmltox-0.12.3_linux-generic-amd64.tar.xz
				#sudo wget http://download.gna.org/wkhtmltopdf/0.12/0.12.1/wkhtmltox-0.12.1_linux-trusty-amd64.deb
				sudo tar -xvf wkhtmltox-0.12.3_linux-generic-amd64.tar.xz
				if [ ! -f /usr/bin/wkhtmltoimage ];then
					sudo mv wkhtmltox/bin/wkhtmltoimage /usr/bin/wkhtmltoimage
				fi
				if [ ! -f /usr/bin/wkhtmltopdf ];then
					sudo mv wkhtmltox/bin/wkhtmltopdf /usr/bin/wkhtmltopdf
				fi
			fi

			#clean repository
			if [ -d wkhtmltox ]; then
				sudo rm wkhtmltox-0.12.3_linux-generic-amd64.tar.xz
				sudo rm -R wkhtmltox
			fi
		}
	
		
		if ([ $erpVersion == "7" ] || [ $erpVersion == "8" ] || [ $erpVersion == "9" ] || [ $erpVersion == "10" ] && [[ $erpVersion =~ $re ]]); then
			#test installed
			#is_wkhtmltopdf_installed
			#if [ -s $USER_HOME/commandrun ]; then
			#	install_wkhtmltopdf
			#fi

			#install wkhtmltopdf
			install_wkhtmltopdf

			if [ ! -d "/opt/$erpUser" ]; then
				sudo mkdir /opt/$erpUser
				sudo chown $erpUser:$erpUser /opt/$erpUser
			fi

		
			#download erp 
			if [ ! -d "/opt/$erpUser/${tabVersionName[0]}" ]; then
				create_erp_erp
			else
				echo "Delete old repository ${tabVersionName[0]} ? [y/n]"
				read deleteerperp
				if ([ $deleteerperp == "y" ] || [ "$deleteerperp" == "Y" ] || [ "$deleteerperp" == "yes" ] || [ "$deleteerperp" == "Yes" ] || [ "$deleteerperp" == "YES" ]); then
					delete_erp_erp
					create_erp_erp
				fi		
			fi

			echo "create erp community repository or delete it"
			create_erp_community (){ 

				  echo
				  echo "Now create repository erp_community really begins."
				  echo

				sudo mkdir /opt/$erpUser/erp_community
				sudo chown "$erpUser":"$erpUser" /opt/$erpUser/erp_community
			}
			delete_erp_community (){ 

				  echo
				  echo "Now delete repository erp_community && venv-erp$erpVersion really begins."
				  echo

				 sudo rm -R "/opt/$erpUser/erp_community" 
				 sudo rm -R "/opt/$erpUser/venv-erpv$erpVersion"
			}
			#create virtual python env
			#install erp and deps
			#ps : For Ubuntu 14.10, replace virtualenv venv-erpv9 par python /usr/lib/python2.7/dist-packages/virtualenv.py venv-erpv9
			#test file erp/ erp_community/ erp_dev/ venv-erpv/ venv-erpv8/  
			if [ ! -d "/opt/$erpUser/erp_community" ]; then
				create_erp_community
			else
				echo "Delete old repository erp_community ? [y/n]"
				read deleteerpCommunity
				if ([ $deleteerpCommunity == "y" ] || [ "$deleteerpCommunity" == "Y" ] || [ "$deleteerpCommunity" == "yes" ] || [ "$deleteerpCommunity" == "Yes" ] || [ "$deleteerpCommunity" == "YES" ]); then
					delete_erp_community
					create_erp_community
				fi		
			fi
		
		
			echo "create repositories for other modules or delete it"
			create_addons_available_addons_enabled (){ 

				  echo
				  echo "Now create repositories addons-available && addons-enabled in erp_community really begins."
				  echo

				  clear			
				  echo "cd ~ && virtualenv venv-erpv"$erpVersion" && source venv-erpv"$erpVersion"/bin/activate && cd erp_community && mkdir addons-{available,enabled} && cd addons-available && git clone --depth 1 --branch "$erpVersion".0 --single-branch https://github.com/OCA/server-tools.git && cd ../addons-enabled && ln -s ../addons-available/server-tools/disable_openerp_online/ && ln -s ../addons-available/server-tools/cron_run_manually/" > $USER_HOME/commandrun
				cmd=$(head -n 1 $USER_HOME/commandrun)
				sudo -H -u "$erpUser" /bin/bash -c "$cmd"
			}
			delete_addons_available_addons_enabled (){ 

				  echo
				  echo "Now delete repositories addons-available && addons-enabled in erp_community really begins."
				  echo

				  sudo rm -R "/opt/$erpUser/erp_community/addons-available" 
				  sudo rm -R "/opt/$erpUser/erp_community/addons-enabled"
			}
			#create repositories for other modules (addons-available && addons-enabled in erp_community )
			if [ ! -d "/opt/$erpUser/erp_community/addons-available" ]; then
				create_addons_available_addons_enabled
			else
				echo "Delete old repositories addons-available && addons_enabled ? [y/n]"
				read deleteOldaddonsAvailableaddonsEnabled
				if ([ $deleteOldaddonsAvailableaddonsEnabled == "y" ] || [ "$deleteOldaddonsAvailableaddonsEnabled" == "Y" ] || [ "$deleteOldaddonsAvailableaddonsEnabled" == "yes" ] || [ "$deleteOldaddonsAvailableaddonsEnabled" == "Yes" ] || [ "$deleteOldaddonsAvailableaddonsEnabled" == "YES" ]); then
					delete_addons_available_addons_enabled
					create_addons_available_addons_enabled
				fi
			fi

			echo "create repository for dev modules"
			create_erp_dev_addons (){ 

				  echo
				  echo "Now create repositories available && enabled in erp_dev really begins."
				  echo

				  echo "cd ~ && virtualenv venv-erpv"$erpVersion" && source venv-erpv"$erpVersion"/bin/activate && cd ~ && mkdir -p erp_dev" > $USER_HOME/commandrun
				  cmd=$(head -n 1 $USER_HOME/commandrun)
				  sudo -H -u "$erpUser" /bin/bash -c "$cmd"
			}
			delete_erp_dev_addons (){ 

				  echo
				  echo "Now delete repository erp_dev ."
				  echo

				 sudo rm -R "/opt/$erpUser/erp_dev" 
			}
			#create repository for dev
			if [ ! -d "/opt/$erpUser/erp_dev" ]; then
				create_erp_dev_addons
			else
				echo "Delete old repository addons erp_dev ? [y/n]"
				read deleteOldaddonsDev
				if ([ $deleteOldaddonsDev == "y" ] || [ "$deleteOldaddonsDev" == "Y" ] || [ "$deleteOldaddonsDev" == "yes" ] || [ "$deleteOldaddonsDev" == "Yes" ] || [ "$deleteOldaddonsDev" == "YES" ]); then
					create_erp_dev_addons
					create_erp_dev_addons
				fi
			fi		

			echo "create repositories in dev modules"
			create_erp_dev_addons_available_addons_enabled (){ 

				  echo
				  echo "Now create repositories available && enabled in erp_dev really begins."
				  echo

				  echo "cd ~ && virtualenv venv-erpv"$erpVersion" && source venv-erpv"$erpVersion"/bin/activate && cd ~ && mkdir -p erp_dev/addons-{available,enabled}" > $USER_HOME/commandrun
				  cmd=$(head -n 1 $USER_HOME/commandrun)
				  sudo -H -u "$erpUser" /bin/bash -c "$cmd"
			}
			delete_erp_dev_addons_available_addons_enabled (){ 

				  echo
				  echo "Now delete repositories addons-available addons-enabled in erp_dev ."
				  echo

				sudo rm -R "/opt/$erpUser/erp_dev/addons-available" 
				sudo rm -R "/opt/$erpUser/erp_dev/addons-enabled"
			}
			#create repository for dev
			#test files 
			if [ ! -d "/opt/$erpUser/erp_dev/addons-available" ]; then
				create_erp_dev_addons_available_addons_enabled
			else
				echo "Delete old repositories addons-available && addons-enabled in erp_dev ? [y/n]"
				read deleteOldaddonsAvailableaddonsEnabledDev
				if ([ $deleteOldaddonsAvailableaddonsEnabledDev == "y" ] || [ "$deleteOldaddonsAvailableaddonsEnabledDev" == "Y" ] || [ "$deleteOldaddonsAvailableaddonsEnabledDev" == "yes" ] || [ "$deleteOldaddonsAvailableaddonsEnabledDev" == "Yes" ] || [ "$deleteOldaddonsAvailableaddonsEnabledDev" == "YES" ]); then
					delete_erp_dev_addons_available_addons_enabled
					create_erp_dev_addons_available_addons_enabled
				fi
			fi
			echo ""
			echo "*** What is your database name ***"
			read postgresDbName
			echo ""
			echo "*** What is your password ***"
			read erpPostgresPwd
			echo ""
			echo "*** What is your master postgres password ***"
			read postgresPwd
			echo ""
			#create postgres db
		
			psql=( psql -v ON_ERROR_STOP=1 )
			postgres='postgres'
			if [ ! -z $postgresDbName ]; then
				#"${psql[ $postgres ]}" --username postgres <<-EOSQL
				#	CREATE DATABASE "$postgresDbName" ENCODING = 'LATIN1' LC_COLLATE = 'fr_FR' LC_CTYPE = 'fr_FR' CONNECTION LIMIT = 2 ;
				#EOSQL
				#echo
				# Adjust PostgreSQL configuration so that remote connections to the
				# database are possible. 
			
				# And add ``listen_addresses`` to ``/etc/postgresql/x.x/main/postgresql.conf``
				postgresql_version
				cmd=$(head -n 1 $USER_HOME/commandrun) 
				echo "local	all	postgres	ident" >> /etc/postgresql/$cmd/main/pg_hba.conf
				echo "listen_addresses = '*' ">> /etc/postgresql/$cmd/main/postgresql.conf
				/etc/init.d/postgresql restart
				echo ""
				echo "*************** Run those instructions ********************"
				echo "$ psql or exit"
				echo "*** CREATE USER "$erpUser" WITH ENCRYPTED PASSWORD '$erpPostgresPwd'; ***"
				echo "*** ALTER ROLE "$erpUser" LOGIN; ***"
				echo "*** ALTER USER $erpUser CREATEUSER CREATEDB REPLICATION CREATEROLE; ***"
				echo "*** CREATE DATABASE $postgresDbName WITH OWNER = $erpUser TEMPLATE = template0 CONNECTION LIMIT = 2; ***"
				echo ""
				echo "===== * Check rights * ====="
				echo "postgres-# \du"
				echo "postgres-# \l"
				echo "postgres-# \q"
				echo "***************  			     ********************"
				echo ""
			
				#pwd for erp connexion
				if [ ! -f /opt/$erpUser/"$erpUser"_user_configuration ]; then
					echo -e "******** $erpUser CONFIGURATION *********\n" > /opt/$erpUser/"$erpUser"_user_configuration
					sudo chown $erpUser:$erpUser /opt/$erpUser/"$erpUser"_user_configuration
				fi
				echo -e "******* DataBase *******\n" >> /opt/$erpUser/"$erpUser"_user_configuration
				echo -e "DB : $postgresDbName \nuser : $erpUser \nuserpwd $erpPostgresPwd \nmaster pwd : $postgresPwd\n" >> /opt/$erpUser/"$erpUser"_user_configuration
				echo -e "== connexion (with local system user) ==\n" >> /opt/$erpUser/"$erpUser"_user_configuration
				echo -e "sudo -i -u $erpUser\n" >> /opt/$erpUser/"$erpUser"_user_configuration
				echo -e "psql $postgresDbName\n" >> /opt/$erpUser/"$erpUser"_user_configuration
						
			
				#sudo -u postgres psql
				sudo su - postgres
				#sudo -u postgres psql -c "CREATE DATABASE $postgresDbName WITH ENCODING 'UTF8' TEMPLATE template1 CONNECTION LIMIT = 2"
				#sudo su - postgres psql postgres -c "CREATE DATABASE $postgresDbName WITH ENCODING 'UTF8' TEMPLATE template0 LC_COLLATE = 'fr_FR' LC_CTYPE = 'fr_FR' CONNECTION LIMIT = 2"
				#sudo -u postgres -H -- psql -d "$postgresDbName" -c "$op USER "$erpUser" WITH SUPERUSER $erpPostgresPwd"
			fi
		

			#> amazingerp
			#$ psql
			#> grant all privileges on database erp to erp;
			#erp configuration
			#test file
			echo "create erp-server.conf file or delete it"
			create_erp_server_conf (){ 

				  echo
				  echo "Now create erp-server.conf really begins."
				  echo
			
				if [ ! -d /opt/$erpUser/venv-erpv$erpVersion ]; then
					echo "cd ~ && virtualenv venv-erpv"$erpVersion" " > $USER_HOME/commandrun
					cmd=$(head -n 1 $USER_HOME/commandrun)
					sudo -H -u "$erpUser" /bin/bash -c "$cmd"
				fi
				clear
				echo "cd ~ && source venv-erpv"$erpVersion"/bin/activate && git clone https://gillesstephane@bitbucket.org/gillesstephane/odooconf.git" > $USER_HOME/commandrun
				cmd=$(head -n 1 $USER_HOME/commandrun)
				sudo -H -u "$erpUser" /bin/bash -c "$cmd"
				sudo -H -u "$erpUser" /bin/bash -c "cp /opt/$erpUser/odooconf/odoo-server.conf /opt/$erpUser/"
			}
			delete_erp_server_conf (){ 

				  echo
				  echo "Now delete repository erpconf && erp-server.conf really begins."
				  echo
				#delete symb link
				sudo rm "/opt/$erpUser/odoo-server.conf"
				sudo rm -R "/opt/$erpUser/odooconf"
			}
			#create virtual python env
			#install erp and deps
			#ps : For Ubuntu 14.10, replace virtualenv venv-erpv9 par python /usr/lib/python2.7/dist-packages/virtualenv.py venv-erpv9
			#test file erp/ erp_community/ erp_dev/ venv-erpv/ venv-erpv8/  
			if [ ! -d "/opt/$erpUser/odooconf" ]; then
				create_erp_server_conf
			else
				echo "Delete old erp-server.conf ? [y/n]"
				read deleteerperpserverconf
				if ([ $deleteerperpserverconf == "y" ] || [ "$deleteerperpserverconf" == "Y" ] || [ "$deleteerperpserverconf" == "yes" ] || [ "$deleteerperpserverconf" == "Yes" ] || [ "$deleteerperpserverconf" == "YES" ]); then
					delete_erp_server_conf
					create_erp_server_conf
				fi		
			fi
		
		
			echo "create erp-server.log file or delete it"
			create_erp_server_log (){ 

				  echo
				  echo "Now create erp-server.log really begins."
				  echo

				echo "sudo touch /opt/$erpUser/erp-server.log" > $USER_HOME/commandrun
				cmd=$(head -n 1 $USER_HOME/commandrun)
				sudo -H -u root /bin/bash -c "$cmd"
				sudo chown "$erpUser":"$erpUser" /opt/$erpUser/erp-server.log
			}
			delete_erp_server_log (){ 

				  echo
				  echo "Now delete repository erp-server.log really begins."
				  echo

				sudo rm "/opt/$erpUser/erp-server.log"
			}
			#create erp log file
			if [ ! -f "/opt/$erpUser/erp-server.log" ]; then
				create_erp_server_log
			else
				echo "Delete old erp-server.log ? [y/n]"
				read deleteerperpserverlog
				if ([ $deleteerperpserverlog == "y" ] || [ "$deleteerperpserverlog" == "Y" ] || [ "$deleteerperpserverlog" == "yes" ] || [ "$deleteerperpserverlog" == "Yes" ] || [ "$deleteerperpserverlog" == "YES" ]); then
					delete_erp_server_log
					create_erp_server_log
				fi		
			fi
		
			#log repository for supervisor
			if [ ! -d /var/$erpUser ]; then
				sudo mkdir /var/$erpUser
				if [ -d /var/$erpUser ]; then
					if [ ! -d /var/$erpUser/supervisorlog ]; then
						sudo mkdir /var/$erpUser/supervisorlog
						if [ -d /var/$erpUser/supervisorlog ]; then
							if [ ! -f /var/$erpUser/supervisorlog/supervisord-$erpUser-server.log ]; then
								sudo echo "server log !" > /var/$erpUser/supervisorlog/supervisord-$erpUser-server.log
							fi
							if [ ! -f /var/$erpUser/supervisorlog/supervisord-$erpUser-server-longpolling.log ]; then
								sudo echo "server longpolling log !" > /var/$erpUser/supervisorlog/supervisord-$erpUser-server-longpolling.log
							fi
						fi
					fi
				fi
				sudo chown -R $erpUser:$erpUser /var/$erpUser
			else
				sudo chown -R $erpUser:$erpUser /var/$erpUser		
			fi
		
			echo "if [ -f /opt/$erpUser/odoo-server.conf ]; then
			    	echo \"erp-server.conf exists.\"
			    	echo -e \"* Change server config file\"
				sudo sed -i s/\"db_user = .*\"/\"db_user = $erpUser\"/g /opt/$erpUser/odoo-server.conf
				sudo sed -i s/\"db_name = .*\"/\"db_name = $postgresDbName\"/g /opt/$erpUser/odoo-server.conf
				sudo sed -i s/\"dbfilter = .*\"/\"dbfilter = ^$erpUser\"/g /opt/$erpUser/odoo-server.conf
				sudo sed -i s/\"db_password = .*\"/\"db_password = $erpPostgresPwd\"/g /opt/$erpUser/odoo-server.conf
				sudo sed -i s/\"admin_passwd = .*\"/\"admin_passwd = $postgresPwd\"/g /opt/$erpUser/odoo-server.conf
			
				if  [ $erpVersion == \"7\" ]; then
					sudo sed -i s/\"addons_path .*\"/\"addons_path = \/opt\/$erpUser\/${tabVersionName[0]}\/openerp\/addons,\/opt\/$erpUser\/erp_community\/addons-enabled,\/opt\/$erpUser\/erp_dev\/addons-enabled\"/g /opt/$erpUser/odoo-server.conf
				else
					sudo sed -i s/\"addons_path .*\"/\"addons_path = \/opt\/$erpUser\/${tabVersionName[0]}\/addons,\/opt\/$erpUser\/erp_community\/addons-enabled,\/opt\/$erpUser\/erp_dev\/addons-enabled\"/g /opt/$erpUser/odoo-server.conf
				fi

				sudo sed -i s/\"; logfile = .*\"/\"logfile = /opt/$erpUser/erp-server.log\"/g /opt/$erpUser/odoo-server.conf
				sudo chmod 600 /opt/$erpUser/odoo-server.conf 

			
				#run supervisor for erp erp-server and erp-server-longpolling
				if [ ! -f /etc/supervisor/conf.d/supervisor-odoo-server.conf ]; then
					clear
					sudo git clone https://gillesstephane@bitbucket.org/gillesstephane/supervisorodooserver.git /etc/supervisor/conf.d/gitsupervisor
					sudo cp /etc/supervisor/conf.d/gitsupervisor/supervisor-odoo-server-longpolling.conf /etc/supervisor/conf.d/
					sudo cp /etc/supervisor/conf.d/gitsupervisor/supervisor-odoo-server.conf /etc/supervisor/conf.d/
					#remove var gitsupervisor repository
					sudo rm -R /etc/supervisor/conf.d/gitsupervisor
				
				else 
				    echo \"Files supervisor-odoo-server.conf && supervisor-odoo-server-longpolling.conf exist\"
				fi

				#test server and polling files
				if [ -f /etc/supervisor/conf.d/supervisor-odoo-server.conf ]; then
			    		echo \"supervisor-odoo-server.conf exists.\"
					#this cmd bug so replace after
					# sudo sed -i s/\"\[program\:supervisor-erp-server]\"/\"\[program\:supervisor-$erpUser-server]\"/g /etc/supervisor/conf.d/supervisor-odoo-server.conf
				
					#special for v10
					if [ $erpVersion == 10 ]; then
						sed -i s/\"command=.*\"/\"command=\/opt\/$erpUser\/venv-erpv$erpVersion\/bin\/python \/opt\/$erpUser\/${tabVersionName[0]}\/odoo-bin -c \/opt\/$erpUser\/odoo-server.conf --logfile=\/var\/$erpUser\/erp-server.log\"/g /etc/supervisor/conf.d/supervisor-odoo-server.conf
					elif [ $erpVersion == 7 ]; then
						sed -i s/\"command=.*\"/\"command=\/opt\/$erpUser\/venv-erpv$erpVersion\/bin\/python \/opt\/$erpUser\/${tabVersionName[0]}\/openerp-server -c \/opt\/$erpUser\/odoo-server.conf --logfile=\/var\/$erpUser\/erp-server.log\"/g /etc/supervisor/conf.d/supervisor-odoo-server.conf				
					else
						sed -i s/\"command=.*\"/\"command=\/opt\/$erpUser\/venv-erpv$erpVersion\/bin\/python \/opt\/$erpUser\/${tabVersionName[0]}\/openerp-server -c \/opt\/$erpUser\/odoo-server.conf --logfile=\/var\/$erpUser\/supervisorlog\/supervisord-${tabVersionName[0]}-server.log\"/g /etc/supervisor/conf.d/supervisor-odoo-server.conf
					fi

					sed -i s/\"user=.*\"/\"user=$erpUser\"/g /etc/supervisor/conf.d/supervisor-odoo-server.conf
					sed -i s/\"directory=.*\"/\"directory=\/opt\/$erpUser\/${tabVersionName[0]}\"/g /etc/supervisor/conf.d/supervisor-odoo-server.conf
					sudo sed -i s/\"environment = .*\"/\"environment = HOME=\\\"\/opt\/$erpUser\/\\\",USER=\\\"$erpUser\\\"\"/g /etc/supervisor/conf.d/supervisor-odoo-server.conf
					#sudo sed -i s/\"logfile=.*\"/\"logfile=\/var\/$erpUser\/supervisorlog\/supervisord-${tabVersionName[0]}-server.log\"/g /etc/supervisor/conf.d/supervisor-odoo-server.conf
				else 
				    echo \"Files supervisor-odoo-server.conf doesn t exists\"
				fi

				if [ -f /etc/supervisor/conf.d/supervisor-odoo-server-longpolling.conf ]; then
			    		echo \"odoo-server.conf doesn t exists.\"
					#this cmd bug so replace after
					#sudo sed -i s/\"\[program\:supervisor-erp-server-longpolling]\"/\"\[program\:supervisor-$erpUser-server-longpolling]\"/g /etc/supervisor/conf.d/supervisor-odoo-server-longpolling.conf
					if [ $erpVersion == 10 ]; then
						sed -i s/\"command=.*\"/\"command=\/opt\/$erpUser\/venv-erpv$erpVersion\/bin\/python \/opt\/$erpUser\/${tabVersionName[0]}\/odoo-bin -c \/opt\/$erpUser\/odoo-server.conf --logfile=\/opt\/$erpUser\/erp-server-longpolling.log\"/g /etc/supervisor/conf.d/supervisor-odoo-server-longpolling.conf
					elif [ $erpVersion == 7 ]; then
						sed -i s/\"command=.*\"/\"command=\/opt\/$erpUser\/venv-erpv$erpVersion\/bin\/python \/opt\/$erpUser\/${tabVersionName[0]}\/openerp-server -c \/opt\/$erpUser\/odoo-server.conf --logfile=\/var\/$erpUser\/erp-server.log\"/g /etc/supervisor/conf.d/supervisor-odoo-server.conf								
					else
						sed -i s/\"command=.*\"/\"command=\/opt\/$erpUser\/venv-erpv$erpVersion\/bin\/python \/opt\/$erpUser\/${tabVersionName[0]}\/openerp-gevent -c \/opt\/$erpUser\/odoo-server.conf --logfile=\/var\/$erpUser\/supervisorlog\/supervisord-erp-server-longpolling.log\"/g /etc/supervisor/conf.d/supervisor-odoo-server-longpolling.conf
					fi

					sed -i s/\"user=.*\"/\"user=$erpUser\"/g /etc/supervisor/conf.d/supervisor-odoo-server-longpolling.conf
					sed -i s/\"directory=.*\"/\"directory=\/opt\/$erpUser\/${tabVersionName[0]}\"/g /etc/supervisor/conf.d/supervisor-odoo-server-longpolling.conf
					sudo sed -i s/\"environment = .*\"/\"environment = HOME=\\\"\/opt\/$erpUser\/\\\",USER=\\\"$erpUser\\\"\"/g /etc/supervisor/conf.d/supervisor-odoo-server-longpolling.conf
					#sudo sed -i s/\"logfile=.*\"/\"logfile=\/var\/$erpUser\/supervisorlog\/supervisord-erp-server-longpolling.log\"/g /etc/supervisor/conf.d/supervisor-odoo-server-longpolling.conf

				else 
				    echo \"Files erp-server-longpolling doesn't exists\"
				fi

		
			else 
			    echo \"File erp-server-conf doesn't exist\"
			    #exec service ssh restart
			    #/usr/sbin/sshd -D
			fi" > $USER_HOME/commandrun
			cmd=$(head -n 100 $USER_HOME/commandrun)
			sudo -H -u root /bin/bash -c "$cmd"

			configure_server_supervisor() {
				#TODO : see how to optimize this after
				#this is for have multiple instances
				#correction seb bug cmd before
				#copy and delete first line, put it on tmp , add new first line file and move it to supervisor
				if [[ ! -z $1 && $1 == "server" ]]; then
					sudo mv /etc/supervisor/conf.d/supervisor-odoo-server.conf /etc/supervisor/conf.d/supervisor-$erpUser-server.conf
					sudo touch /opt/$erpUser/supervisor-$erpUser-server.conf 
					sudo sed '1d' /etc/supervisor/conf.d/supervisor-$erpUser-server.conf > /opt/$erpUser/supervisor-$erpUser-server.conf
					sudo sed -i "1 i\[program\:supervisor-$erpUser-server]" /opt/$erpUser/supervisor-$erpUser-server.conf
					sudo mv /opt/$erpUser/supervisor-$erpUser-server.conf /etc/supervisor/conf.d/supervisor-$erpUser-server.conf			
				else
					if [[ ! -z $1 && $1 == "longpolling" ]]; then
						sudo mv /etc/supervisor/conf.d/supervisor-odoo-server-longpolling.conf /etc/supervisor/conf.d/supervisor-$erpUser-server-longpolling.conf
						#correction seb bug cmd before
						#copy and delete first line, put it on tmp , add new first line file and move it to supervisor
						sudo touch /opt/$erpUser/supervisor-$erpUser-server-$1.conf 
						sudo sed '1d' /etc/supervisor/conf.d/supervisor-$erpUser-server-$1.conf > /opt/$erpUser/supervisor-$erpUser-server-$1.conf ; 
						sudo sed -i "1 i\[program\:supervisor-$erpUser-server-$1]" /opt/$erpUser/supervisor-$erpUser-server-$1.conf
						sudo mv /opt/$erpUser/supervisor-$erpUser-server-$1.conf /etc/supervisor/conf.d/supervisor-$erpUser-server-$1.conf
					fi			
				fi
			}

		

			configure_server_supervisor "longpolling"
			configure_server_supervisor "server"


			#Nginx
			#put erp derrière un Reverse Proxy HTTP.
			apt-get remove apache2 -y
			echo "create erp-server.log file or delete it"
			create_nginx (){ 

				  echo
				  echo "Now create nginx really begins."
				  echo
				clear
				sudo git clone https://gillesstephane@bitbucket.org/gillesstephane/nginx.git /etc/nginx/sites-available/clonenginx
				sudo cp /etc/nginx/sites-available/clonenginx/nginx-xxxx.conf /etc/nginx/sites-available/nginx-$erpUser.conf
				sudo rm /etc/nginx/sites-available/clonenginx/nginx-xxxx.conf
				if [ ! -L /etc/nginx/sites-enabled/nginx-$erpUser.conf ];then
					cd /etc/nginx/sites-enabled && sudo ln -s ../etc/nginx/sites-available/nginx-$erpUser.conf 
				fi
				sudo rm -R /etc/nginx/sites-available/clonenginx
			}
			delete_nginx (){ 

				  echo
				  echo "Now delete repository nginx really begins."
				  echo
				if [ -d /etc/nginx/sites-available/clonenginx ]; then
					sudo rm -R /etc/nginx/sites-available/clonenginx
					sudo rm /etc/nginx/sites-available/nginx-$erpUser.conf
					if [ -L /etc/nginx/sites-available/nginx-$erpUser.conf ]; then
						echo "/etc/nginx/sites-available/nginx-$erpUser.conf~"
						#sudo rm /etc/nginx/sites-available/erp"$erpUser".conf~
					fi
				fi
			
			}
			#create erp log file
			if [ ! -d /etc/nginx/sites-available/clonenginx ]; then
				create_nginx
			else
				echo "Delete old nginx ? [y/n]"
				read deleteoldnginx
				if ([ $deleteoldnginx == "y" ] || [ "$deleteoldnginx" == "Y" ] || [ "$deleteoldnginx" == "yes" ] || [ "$deleteoldnginx" == "Yes" ] || [ "$deleteoldnginx" == "YES" ]); then
					delete_nginx
					create_nginx
				fi		
			fi
		
		
			#change config
			echo "Change proxy port ? [default: 8069]"
			read proxyPort
			if [ ! -z "$proxyPort" ] ; then
				if ! [[ $proxyPort =~ $re ]] ; then
				   	echo "error: Not a number" >&2; exit 1
				else
					echo "sed -i s/\"proxy_pass http\:\\/\\/127.0.0.1:8069\"/\"proxy_pass http\:\\/\\/127.0.0.1:$proxyPort\"/g /etc/nginx/sites-available/nginx-$erpUser.conf" > $USER_HOME/commandrun
				  	cmd=$(head -n 100 $USER_HOME/commandrun)
					sudo -H -u root /bin/bash -c "$cmd" 
		                        sudo echo "xmlrpc_port = $proxyPort" >> /opt/$erpUser/odoo-server.conf
				fi
			fi

			echo "Change proxy longpolling port ? [default: 8072]"
			read proxyPortLongpolling
			if [ ! -z "$proxyPortLongpolling" ] ; then
				if ! [[ $proxyPortLongpolling =~ $re ]] ; then
				   echo "error: Not a number" >&2; exit 1
				else
				  	echo "sed -i s/\"proxy_pass http\:\\/\\/127.0.0.1:8072\"/\"proxy_pass http\:\\/\\/127.0.0.1:$proxyPortLongpolling\"/g /etc/nginx/sites-available/nginx-$erpUser.conf" > $USER_HOME/commandrun
					cmd=$(head -n 100 $USER_HOME/commandrun)
					sudo -H -u root /bin/bash -c "$cmd"
		                        sudo echo "longpolling_port = $proxyPortLongpolling" >> /opt/$erpUser/odoo-server.conf
				fi
			fi
			#rm defaut
			sudo service nginx configtest
			sudo service nginx restart
		
			#add to config file
			echo -e "\n\n ******** PORT NGINX ACCESS ****** \n odoo-server : $proxyPort \n odoo-server-longpolling : $proxyPortLongpolling \n" >> /opt/$erpUser/"$erpUser"_user_configuration

			data_config (){
				echo 
				if [ $erpVersion == 10 ]; then
					echo -e "\n\n*************** TEST *******************\n== For test your install you can also run those cmd ==\n $ sudo su - $erpUser -s /bin/bash\n $ source venv-erpv$erpVersion/bin/activate\n $ python odoo/odoo-bin -c odoo-server.conf\n " >> /opt/$erpUser/"$erpUser"_user_configuration
				else
					echo -e "\n\n*************** Add $erpUser user password *******************\n == Don't forget to add user password ==\n By default system user don't have password \n command to run for add password \n  $ sudo su\n  # passwd $erpUser\n  # enter password\n  # exit\n $ su - $erpUser " >> /opt/$erpUser/"$erpUser"_user_configuration
				fi

				echo -e "\n\n********** For Dev module (pycharm) *************\n  == for dev give $erpUser rights access (NB :that is not good way) ==\n $ sudo chmod -R a+rwx /opt/$erpUser/" >> /opt/$erpUser/"$erpUser"_user_configuration
	 		}

			data_config
			less /opt/$erpUser/"$erpUser"_user_configuration
		
			clone_git_remote() {
				while true; do
				    	echo "Do you want to clone somme repositories on github [1 = yes / 0 = no]:"
					read remotecloned

					case $remotecloned in
	     					1)
							echo "What is local repository name:"
							read localrepo
							if [ ! -z $localrepo ]; then
								#clear file content
								> $USER_HOME/commandrun
								echo "Enter git repository address "
								read gitremote
								if [ ! -z $gitremote ]; then
									#check remote exist
									sudo git ls-remote $gitremote > $USER_HOME/commandrun
									if [ -s $USER_HOME/commandrun ]; then
										sudo mkdir /opt/$erpUser/erp_dev/addons-available/$localrepo
										clear
										sudo git clone $gitremote /opt/$erpUser/erp_dev/addons-available/$localrepo
										sudo chown -R $erpUser:$erpUser /opt/$erpUser/erp_dev/addons-available/$localrepo

										if [ "$(ls -A /opt/$erpUser/erp_dev/addons-available/$localrepo)" ]; then
											echo "*** your remote has been cloned on /opt/$erpUser/erp_dev/addons-available/$localrepo"
											clear
											echo "Do yo want to see content ?"
											read response
											if ([ $response == "y" ] || [ "$response" == "Y" ] || [ "$response" == "yes" ] || [ "$response" == "Yes" ] || [ "$response" == "YES" ]); then
												sudo ls -ail /opt/$erpUser/erp_dev/addons-available/$localrepo
											fi
										else
										   echo "*** WARNING : GIT cannot clone the repository ! try another"
										fi
									fi
								fi
							fi
							;;
						0)
						     echo "OK, that is your choice no repository cloned !"
						     break
						     ;;
						*)
						     echo "That is not a valid choice ! try a number from 0 to 1."
						     ;;
				       esac  
				done
			}
		
			copy_content_to_bashrc() {
				> $USER_HOME/commandrun
				#test if function exit in ~/.bashrc			
				if grep -q 'which_*' $USER_HOME/.bashrc; then
				    echo "found"  > $USER_HOME/commandrun
				else
				    echo "notfound" > $USER_HOME/commandrun
				fi

				cmd=$(head -n 1 $USER_HOME/commandrun)
				if [ ! -z "$cmd" ];then
					if [ "$cmd" = "notfound" ];then
						sudo mkdir /opt/$erpUser/which_term
						if [ -d /opt/$erpUser/which_term ]; then
							rep=/opt/$erpUser/which_term
							clear
							sudo git clone https://gmeyomesse@bitbucket.org/odoodeployment/whichterm.git $rep
						    	value=$(<$rep/which_term)
							sudo echo "$value" >> $USER_HOME/.bashrc
							#remove clone repo
							sudo rm -R $rep/which_term
						fi
					fi	
				fi
			}

			run_ungit() {
				# run ungit in new term
				#run ungit on special dev repository
				source ~/.bashrc
				clear
				while true; do
					if [ ! -s $USER_HOME/commandrun ]
					then
						sleep 1
					else
						#default create sample git dev directory (for ungit demo)
						sudo mkdir /opt/$erpUser/erp_dev/addons-available/sample 
						sudo git init /opt/$erpUser/erp_dev/addons-available/sample 
						sudo chown -R $erpUser:$erpUser /opt/$erpUser/
						break
					fi
				done
				run_supervisor
				cmd=$(head -n 1 $USER_HOME/commandrun)
				sudo -H -u `whoami` /bin/bash -c "cd /opt/$erpUser/erp_dev/addons-available/sample && gnome-terminal -e ungit"
				exit
			}
		
			clear
			echo 
			echo "Do you want tools for dev (pycharm community) [y/n]:"
			read response
			if ([ $response == "y" ] || [ "$response" == "Y" ] || [ "$response" == "yes" ] || [ "$response" == "Yes" ] || [ "$response" == "YES" ]); then
				sudo add-apt-repository ppa:mystic-mirage/pycharm
				sudo apt update
				#sudo apt install pycharm
				#for the community edition
				sudo apt-get -y install pycharm-community
				#sudo apt remove pycharm pycharm-community
				echo 
				echo "******** Run : $ pycharm-community *********"
				echo

				#clone git remotes
				clone_git_remote
			fi

			clear
			echo 
			echo "Do you want tools for dev (ungit) [y/n]:"
			read response
			if ([ $response == "y" ] || [ "$response" == "Y" ] || [ "$response" == "yes" ] || [ "$response" == "Yes" ] || [ "$response" == "YES" ]); then
						
				#first install nodejs	
				#don't use ubuntu repo for install nodejs
				# https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions		
				sudo curl -sL https://deb.nodesource.com/setup_7.x | sudo -E bash -
				sudo apt-get install -y nodejs
				#important configure nodejs for run it
				if [ -L /usr/local/bin/node ]; then
				  	echo "=> File doesn't exist"
				    	sudo rm /usr/local/bin/node
					sudo rm /usr/local/bin/npm
				fi
			
				#install ungit
				sudo npm install -g ungit
				#after install this create 2 symb links
				#/usr/local/bin/ungit -> /usr/local/lib/node_modules/ungit/bin/ungit
				#/usr/local/bin/0ungit-credentials-helper -> /usr/local/lib/node_modules/ungit/bin/credentials-helper
				#you have node module on /usr/local/lib (librairies locales node_modules/python2.7/python3.5)
			
				echo 
				echo "******** Run : $ ungit *********"
				echo

				#add which_term function on .bashrc
				copy_content_to_bashrc 	

			fi

			clear
			echo
			echo "Do you want pgadmin ? [y/n]"
			read response
			if ([ $response == "y" ] || [ "$response" == "Y" ] || [ "$response" == "yes" ] || [ "$response" == "Yes" ] || [ "$response" == "YES" ]); then
				pgadmin_installed
				if [ -s $USER_HOME/commandrun ]; then
					sudo apt-get -y install pgadmin3
				fi
			fi
		
			manual_supervisor() {
				#In some versions of untuntu
				#The supervisord process must be manually restarted with sudo supervisord
				#except id one odoo instance already run
				sudo supervisord
			}

			is_instance_erp_supervisor() {
				if [ ${#tabVersionName[@]} -gt 2 ]; then
					if [ ! -f /etc/supervisor/conf.d/supervisor-${tabVersionName[2]}-server.conf ]; then
						echo "---- manual run supervisor ------"
						manual_supervisor
					fi
				fi
			}

			run_supervisor() {
				is_instance_erp_supervisor
				sudo supervisorctl reload
				sudo supervisorctl reread
				sudo supervisorctl update
				sudo supervisorctl restart supervisor-$erpUser-server && sudo supervisorctl restart supervisor-$erpUser-server-longpolling
			}

			clear
			echo 
			echo "Do you want to run ungit [y/n]:"
			read response
			if ([ $response == "y" ] || [ "$response" == "Y" ] || [ "$response" == "yes" ] || [ "$response" == "Yes" ] || [ "$response" == "YES" ]); then
				run_ungit
			else
				run_supervisor
			fi
		else
		  echo "Version selected is not availabled."
		fi
	else
		echo "Cannot create 2 users with same name"
	fi

else
  echo "Stop install, have good day =) ."
fi
